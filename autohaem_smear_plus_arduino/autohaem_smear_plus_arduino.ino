/* Sketch to control autohaem smear plus */

// Include the AccelStepper library:
#include <AccelStepper.h>

// Define stepper motor connections and motor interface type. Motor interface type must be set to 1 when using a driver:
#define dirPin 2
#define stepPin 3
#define enablePin 4
#define buttonPin 5
#define switchPin 6
#define ledPin 13 
#define motorInterfaceType 1

// Create a new instance of the AccelStepper class:
AccelStepper stepper = AccelStepper(motorInterfaceType, stepPin, dirPin);

//Adjust these variables:
float reverseDistance = 2.5; //Distance(cm) moved back slowly from the end of the slide to the point where smearing begins
float bloodSmearWidth = 1; //Distance(cm) travelled further back slowly during the blood smear
//float forwardDistance = 1.1; //Distance(cm) moved forth fast
float reverseSpeed = 1; //Speed (cm/s)moved back
float forwardSpeed = 7; //Speed(cm/s) moved forth

// Variables will change:
int ledState = HIGH;         // the current state of the output pin
int buttonState;             // the current reading from the input pin
int lastButtonState = HIGH;   // the previous reading from the input pin

// the following variables are unsigned longs because the time, measured in
// milliseconds, will quickly become a bigger number than can be stored in an int.
unsigned long lastDebounceTime = 0;  // the last time the output pin was toggled
unsigned long debounceDelay = 50;    // the debounce time; increase if the output flickers


//Define a function that converts distance (cm) to steps:
int cmToStepsConversion(float x){
  float result;
  result = round(x * 250 * 2);
  return result;
}

void setup() {
  Serial.begin(9600); // Start Serial Monitor at Board Rate 9600
  Serial.println("Setup Initiated");


  //Define pin modes and set enable pin
  pinMode(buttonPin, INPUT_PULLUP);
//  digitalWrite(buttonPin, HIGH);
  pinMode(switchPin, INPUT_PULLUP);
  pinMode(ledPin, OUTPUT);
  stepper.setEnablePin(enablePin);
  stepper.setPinsInverted(false, false, true);

  // set initial LED state
  digitalWrite(ledPin, ledState);

  //read the state of the sensor into a local variable;
  int sensorstate = digitalRead(switchPin);
  


  //Initial Calibration
  digitalWrite(ledPin, HIGH);     //Turns on LED
  stepper.enableOutputs();        //Enables Motor Pin Outputs
  stepper.setSpeed(100);
  stepper.setMaxSpeed(1000);
  stepper.setAcceleration(-5000);
  
  while(sensorstate==0){
    sensorstate = digitalRead(switchPin);
    stepper.move(-50);
    stepper.runSpeed();
  }
  
  stepper.setCurrentPosition(-cmToStepsConversion(0.5));  //Sets this position as 0
  stepper.moveTo(0);
  stepper.runToPosition();
  stepper.disableOutputs();       //Disables Motor Pin Outputs
  digitalWrite(ledPin, LOW);      //Turns off LED

   
  Serial.println("Setup Complete");
} 

void loop() {

  
// read the state of the switch into a local variable:
  int reading = digitalRead(buttonPin);

  // check to see if you just pressed the button
  // (i.e. the input went from LOW to HIGH), and you've waited long enough
  // since the last press to ignore any noise:

  // If the switch changed, due to noise or pressing:
  if (reading != lastButtonState) {
    // reset the debouncing timer
    lastDebounceTime = millis();
  }

  if ((millis() - lastDebounceTime) > debounceDelay) {
    // whatever the reading is at, it's been there for longer than the debounce
    // delay, so take it as the actual current state:

    // if the button state has changed:
    if (reading != buttonState) {
      buttonState = reading;

      // only start the motion if the new button state is LOW
      if (buttonState == LOW) {
        Serial.println("Moving Backwards");   //Prints 'Moving Backwards'
        digitalWrite(ledPin, HIGH);           //Turns LED On
        stepper.enableOutputs();              //Enables Motor Pin Outputs
        stepper.setMaxSpeed(cmToStepsConversion(2));                      //Sets Speed Moving Back
        stepper.setAcceleration(5000);        //Sets Acceleration
        stepper.moveTo(cmToStepsConversion(reverseDistance));//Determines Position to Move to (Clockwise)
        stepper.runToPosition();              //Executes Command to Move to that Position    
        stepper.setMaxSpeed(cmToStepsConversion(reverseSpeed));            //Sets Maximum Speed
        stepper.moveTo(cmToStepsConversion(bloodSmearWidth+reverseDistance));                 //Determines Position to Move to (Clockwise)
        stepper.runToPosition();              //Executes Command to Move to that Position
        stepper.setMaxSpeed(cmToStepsConversion(forwardSpeed));            //Sets Maximum Speed on the way back
        stepper.setAcceleration(30000);        //Sets Acceleration on the way back
        stepper.moveTo(0);                    //Sets Original Position for Return
        Serial.println("Moving Forwards");    //Prints 'Moving Forwards'
        stepper.runToPosition();              //Executes Command to Move Back
        Serial.println("Finished");    //Prints 'Moving Forwards'
        stepper.disableOutputs();             //Disables Motor Pin Outputs
        digitalWrite(ledPin, LOW);            //Turns LED Off
      }
    }
  }


  // save the reading. Next time through the loop, it'll be the lastButtonState:
  lastButtonState = reading;
}
