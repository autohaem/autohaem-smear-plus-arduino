# autohaem smear plus Arduino code

The arduino code to power [autohaem smear+](https://gitlab.com/autohaem/autohaem-smear-plus).

Requires the [AccelStepper library](https://www.airspayce.com/mikem/arduino/AccelStepper/).

Instructions for downloading and use can be found in the [autohaem smear+ assembly instructions](https://autohaem.gitlab.io/autohaem-smear-plus/v2.0.0/attach_everything_together.html)

